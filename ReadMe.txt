==========================================================================
      TM/BA Series Printer Driver for Linux Version 2.0.3.0
  Copyright (C) Seiko Epson Corporation 2016. All rights reserved.
==========================================================================


1. Overview

This software is a printer driver for printing on a TM/BA series 
printer from Linux using CUPS.
For details, please refer to the driver manual contained in the 
driver package.


2. Driver package

File name:  tmx-cups-2.0.3.0.tar.gz
Time stamp: 2016/03/17 02:00:00
File type:  Compressed tar archives


3. Installation

Please copy the driver package file to a local directory, and unpack it.
The driver manual is stored in tmx-cups/manual/ directory.
Then please enter the top directory of the unpacked driver package(tmx-cups),
and execute install.sh.
Please refer to the driver manual for details.


End.
